# Introducción a Docker

### Referencias
* [Docker architecture](https://devopedia.org/docker)
* [What is a container](https://www.docker.com/resources/what-container)

### Instalacion de Git Bash (Windows)
* [Descargar e Instalar Docker desde](https://www.docker.com/products/docker-desktop)


### Buscar una imagen
```
docker search mysql
docker search openjdk
docker search ubuntu
```

### Descargar una imagen
```
docker pull ubuntu:20.10
```

### Crear un container desde una imagen y ejecutarla
* Nombrando containers (--name)
```
docker run -i -t --name ubuntu_itana ubuntu:20.10
```

### Agregar GIT a nuestro Ubuntu
```
apt-get update
apt-get install -y git
```

### Guardar nuestra nueva imagen
```
docker commit ubuntu_itana ubuntu_itana_con_git
```



### Trabajando con los containers

#### Listar containers
```
docker ps
docker ps -a
```

#### Iniciar, Detener o Remover un container
```
docker start ubuntu_itana
docker start ubuntu_itana -i
docker stop ubuntu_itana
docker rm ubuntu_itana

```



### Trabajando con las imagenes

#### Listar, inspeccionar o remover imagenges
```
docker image ls
docker image inspect ubuntu_itana_con_git
docker image rm ubuntu_itana_con_git
```


### Trabajando con puertos y volumenes
* -p, --publish <hostport>:<container_port>
* -v, --volume montar un volumen
```
docker run -i -p 8080:80 -v /Users/jcabelloc/workspace/temp/docker:/usr/share/nginx/html --name nginx-website nginx
```

### Trabajando con Dockerfile

#### Crear un archivo Dockerfile
```
FROM nginx:latest

WORKDIR /usr/share/nginx/html

COPY . .

```

#### Generar la imagen
* Tagging imagenes (-t)
```
docker image build -t jcabelloc/nginx-website .
```

#### Iniciar dicha imagen
```
docker run -i -p: 8081:80 jcabelloc/nginx-website .
```

### Caso practico - 1

#### Configurar un container ad-hoc
```
docker run -it -p 3306:3306 -e TZ=America/Lima -e MYSQL_ROOT_PASSWORD=secreto -e MYSQL_DATABASE=app -e MYSQL_USER=app -e MYSQL_PASSWORD=secreto --name app_mysql mysql:5.7.27 --character-set-server=latin1 --collation-server=latin1_general_ci
```
#### Crear una imagen desde dicho container
```
docker commit app_mysql jcabelloc/app_mysql
```
#### Compartir dicha imagen por Docker hub, subiendo dicha imagen
```
docker push jcabelloc/app_mysql
```

#### Obtener la imagen desde Docker Hub
```
docker pull jcabelloc/app_mysql
```

#### Iniciar el container con nuestra imagen
```
 docker run -i -p 3306:3306 --name app jcabelloc/app_mysql
```


### Caso practico - 2

#### Crear Dockerfile
```
FROM mysql:5.7.27
MAINTAINER Juan Cabello <jcabelloc@itana.pe>

ENV TZ America/Lima
ENV MYSQL_ROOT_PASSWORD secreto
ENV MYSQL_DATABASE app
ENV MYSQL_USER app
ENV MYSQL_PASSWORD secreto
CMD ["--character-set-server=latin1", "--collation-server=latin1_general_ci"]

```

#### Crear la imagen desde Dockerfile(estando en la carpeta del archivo Dockerfile)
* Tagging imagenes (-t)
```
docker image build -t jcabelloc/app_mysql .
```

#### Iniciar el container con nuestra imagen
```
 docker run -i -p 3306:3306 --name app jcabelloc/app_mysql
```
