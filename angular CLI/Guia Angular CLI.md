# Angular CLI

### Instalacion
```npm install -g @angular/cli```

### Version
```ng v```

### Crear proyecto
```ng new [nombre]```

### Ejecutar proyecto
```ng serve -o```

### Crear modulo con routing
```ng g module [nombre] --routing=true```

### Crear clase 
```ng g class [nombre]```

### Crear Servicio 
```ng g s [nombre]```

### Crear Componente
```ng g c [nombre]```



