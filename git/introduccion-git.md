# Introducción a Git

### Instalacion de Git Bash (Windows)
* [Descargar e Instalar Git desde](https://git-scm.com/download/win)

### Inicializar un repositorio local en Git
```
git init
```

### Estado del repositorio
```
git status
```

## Flujo de trabajo básico en git

* Se modifica los archivos en el directorio de trabajo.

* Se selecciona los cambios que se desea formen parte del proximo commit. Solo dichos cambios pasan al area staging (Pre-Commit).

* Se realiza un commit, el cual toma los archivos como están en el área staging y los pasa repositorio Git.


### Agregar archivos al area Pre-Commit
* Agregar por nombre de archivo
```
git add <NOMBRE_ARCHIVO>
```
* Agregar todos los archivos modificados
```
git add .
```

### Realizando un Commit
```
git commit -m "Mensaje sucinto del cambio realizado"
```


## Ver los cambios antes de realizar un Commit
```
git diff
```


## Ver el historial de cambios
```
git log
```


## Hacer que git ignore archivos o directorios: Git Ignore
* Archivo: .gitignore
```

```
